# OpenCV streamer app using gstreamer

This code allows to stream the video from one or two local USB camera(s) using gstreamer. If two different cameras are used, this app automatically performs the concatenation of the images such that a single stream is broadcasted instead of two.

This app is compatible to use with [Accuware Dragonfly](https://www.dragonflycv.com/) localization product.

## Requirements

The following packages have to be installed for the application to work properly:

```bash
# Update repositories first
sudo apt update
 
# Install required packages
sudo apt-get -y install git
sudo apt-get -y install libgtk2.0-dev
sudo apt-get -y install yasm
sudo apt-get -y install libgstreamer1.0-dev
sudo apt-get -y install libgstreamer-plugins-base1.0-dev
sudo apt-get -y install gstreamer1.0-libav
sudo apt-get -y install gstreamer1.0-plugins-bad
sudo apt-get -y install gstreamer1.0-tools
sudo apt-get -y install gstreamer1.0-plugins-good
sudo apt-get -y install gstreamer1.0-plugins-ugly
sudo apt-get -y install gstreamer1.0-nice
sudo apt-get -y install cmake
```

We also recommend manually installing `ffmpeg-3.4.2` and `opencv-3.4.1` for guaranteed compatibility.

- Install `ffmpeg-3.4.2`

```bash
wget https://s3.amazonaws.com/navizon.indoors.camera/dragonfly/ffmpeg-3.4.2.tar.xz
tar -xvf ffmpeg-3.4.2.tar.xz
cd ffmpeg-3.4.2
./configure --enable-nonfree --enable-pic --enable-shared
make -j4
sudo make install
```

- Install `opencv-3.4.1`

```bash
wget https://s3.amazonaws.com/navizon.indoors.camera/dragonfly/opencv-3.4.1.zip
unzip opencv-3.4.1.zip
cd opencv-3.4.1
mkdir build
cd build
cmake ..
make -j4
sudo make install
```

## Installation

You can easily clone and compile the app using `git` and `cmake`.

```bash
git clone https://bitbucket.org/DenisAccuware/opencv-streamer.git
cd opencv-streamer
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

## Run the application

Find the executable under the `opencv-streamer/bin` directory. Then run it like so:

- Mono camera streaming

```bash
# Adapt --ip and --port values according to your device current IP and required PORT to use
./OpenCVStreamer --webcam 0 --width 640 --height 480 --ip 192.168.1.5 --port 5555
```

- Dual camera streaming

```bash
# Adapt --ip and --port values according to your device current IP and required PORT to use
./OpenCVStreamer --webcam 0 --webcam_right 2 --width 640 --height 480 --ip 192.168.1.5 --port 5555
```

By default, the video is sent as an MJPEG stream. To switch to accelerated H.264 (Raspberry Pi only), please provide the additional option `--codec acch264`.

_NOTE: Webcam indexes can change depending on the operating system. On Ubuntu 18.04, usually the index is incremented by 2 between each camera. On other system it can be incremented by one, so command might be `--webcam 0 --webcam_right 1`._

## Provider

This code is provided by [Accuware Inc](https://www.accuware.com/).
