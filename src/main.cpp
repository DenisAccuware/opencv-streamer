#include <opencv2/opencv.hpp>
#include <chrono>

using namespace std;

std::vector<long> mvTimestamps;

double GetFPS()
{
    long lTimestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    mvTimestamps.push_back(lTimestamp);

    int cnt = 0;
    auto vit1 = mvTimestamps.begin();
    while(vit1 != mvTimestamps.end())
    {
        long t = *vit1;

        if(t < lTimestamp - 1000)
            vit1 = mvTimestamps.erase(vit1);
        else
        {
            cnt++;
            vit1++;
        }
    }

    return cnt;
}

int main(int argc, char **argv)
{
    // Read input options

    std::vector<std::string> vsValidArgument;

    vsValidArgument.push_back("--webcam");
    vsValidArgument.push_back("--webcam_right");
    vsValidArgument.push_back("--width");
    vsValidArgument.push_back("--height");
    vsValidArgument.push_back("--ip");
    vsValidArgument.push_back("--protocol");
    vsValidArgument.push_back("--port");
    vsValidArgument.push_back("--codec");

    std::map<std::string, std::string> mssOptions;

    // Check if help should be displayed

    if (argc < 2 || std::string(argv[1]) == std::string("--help"))
    {
        cout << "Available options:" << endl
             << "  [--webcam] index of the unique / left webcam for a stereo system" << endl
             << "  [--webcam_right] index of the right webcam for a stereo system" << endl
             << "  [--width] required image width for each webcam" << endl
             << "  [--height] required image width for each webcam" << endl
             << "  [--ip] ip of the 'sender' (most probably the IP of the device running this app)" << endl
             << "  [--protocol] 'tcp' or 'udp' (default is 'tcp')." << endl
             << "  [--port] port of the 'sender'" << endl
             << "  [--codec] 'mjpeg' or 'acch264' (default is 'mjpeg')" << endl;
        return 0;
    }

    // Read arguments

    int idx = 1;
    while (idx < argc)
    {
        std::string sArg = std::string(argv[idx]);

        if (std::find(vsValidArgument.begin(), vsValidArgument.end(), sArg) != vsValidArgument.end())
        {
            idx++;

            if (idx >= argc)
            {
                cerr << "ERROR: missing value after '" << sArg << "'." << endl;
                return -1;
            }

            mssOptions[sArg] = std::string(argv[idx]);
        }
        else
        {
            cerr << "ERROR: option '" << sArg << "' doesn't exist. Please use '--help' for more information." << endl;
            return -1;
        }

        idx++;
    }

    // Get IP address

    std::string sIP = "127.0.0.1";

    if (mssOptions.count("--ip") > 0)
        sIP = mssOptions["--ip"];

    // Get port

    std::string sPort = "5000";

    if (mssOptions.count("--port") > 0)
        sPort = mssOptions["--port"];

    // Get protocol

    std::string sProtocol = "tcp";

    if (mssOptions.count("--protocol") > 0)
        sProtocol = mssOptions["--protocol"];

    // Webcam indexes

    int nLeftIdx = -1, nRightIdx = -1;

    if (mssOptions.count("--webcam") > 0)
        nLeftIdx = std::stoi(mssOptions["--webcam"]);
    if (mssOptions.count("--webcam_right") > 0)
        nRightIdx = std::stoi(mssOptions["--webcam_right"]);

    if (nLeftIdx == -1)
    {
        cerr << "At least the --webcam index should be provided" << endl;
        return 1;
    }

    // Get codec

    std::string sCodec = "mjpeg";

    if (mssOptions.count("--codec") > 0)
        sCodec = mssOptions["--codec"];

    // Webcam size

    int nWidth = 640, nHeight = 480;

    if (mssOptions.count("--width") > 0)
        nWidth = stoi(mssOptions["--width"]);

    if (mssOptions.count("--height") > 0)
        nHeight = stoi(mssOptions["--height"]);

    cv::Size sOutSize;

    if (nRightIdx < 0)
        sOutSize = cv::Size(nWidth, nHeight);
    else
        sOutSize = cv::Size(2 * nWidth, nHeight);

    cv::VideoCapture vcl, vcr;
    vcl.open(nLeftIdx);

    vcl.set(CV_CAP_PROP_FOURCC, CV_FOURCC('M', 'J', 'P', 'G'));
    vcl.set(CV_CAP_PROP_FRAME_WIDTH, nWidth);
    vcl.set(CV_CAP_PROP_FRAME_HEIGHT, nHeight);
    vcl.set(CV_CAP_PROP_FPS, 30);

    if(!vcl.isOpened())
    {
        cerr << "Could not open camera at index: " << nLeftIdx << endl;
        return 1;
    }

    if (nRightIdx >= 0)
    {
        vcr.open(nRightIdx);

        vcr.set(CV_CAP_PROP_FOURCC, CV_FOURCC('M', 'J', 'P', 'G'));
        vcr.set(CV_CAP_PROP_FRAME_WIDTH, nWidth);
        vcr.set(CV_CAP_PROP_FRAME_HEIGHT, nHeight);
        vcr.set(CV_CAP_PROP_FPS, 30);

        if(!vcr.isOpened())
        {
            cerr << "Could not open camera at index: " << nRightIdx << endl;
            return 1;
        }
    }

    std::stringstream ssPipeline;

    if(sCodec == std::string("acch264"))
    {
        if(sProtocol == std::string("udp"))
        {
            ssPipeline << "appsrc ! video/x-raw,format=YUY2,width=" << sOutSize.width << ",height=" << sOutSize.height << ",framerate=30/1 "
               << "! videoscale ! videoconvert ! omxh264enc ! rtph264pay ! udpsink host="
               << sIP << " port=" << sPort;
        }
        else
        {
            ssPipeline << "appsrc ! video/x-raw,format=YUY2,width=" << sOutSize.width << ",height=" << sOutSize.height << ",framerate=30/1 "
               << "! videoscale ! videoconvert ! omxh264enc ! rtph264pay ! gdppay ! tcpserversink host="
               << sIP << " port=" << sPort;
        }
    }
    else
    {
        if(sProtocol == std::string("udp"))
        {
            ssPipeline << "appsrc ! videoconvert ! video/x-raw,format=YUY2,width=" << sOutSize.width << ",height=" << sOutSize.height << ",framerate=30/1 "
               << "! decodebin name=dec ! videoconvert ! jpegenc ! rtpjpegpay ! udpsink host="
               << sIP << " port=" << sPort;
        }
        else
        {
            ssPipeline << "appsrc ! videoconvert ! video/x-raw,format=YUY2,width=" << sOutSize.width << ",height=" << sOutSize.height << ",framerate=30/1 "
               << "! decodebin name=dec ! videoconvert ! jpegenc ! multipartmux ! tcpserversink host="
               << sIP << " port=" << sPort;
        }
    }

    cv::VideoWriter streamer(
        ssPipeline.str(),
        0,
        30,
        sOutSize,
        true);

    if (streamer.isOpened())
    {
        cout << "Stream started" << endl
             << "-> IP: " << sIP << endl
             << "-> PORT: " << sPort << endl << endl;

        cout << "To access the stream:" << endl << endl;

        if(sCodec == std::string("acch264"))
        {
            if(sProtocol == std::string("udp"))
            {
                cout << "gstreamer command-line: " << endl
                    << "-> gst-launch-1.0 udpsrc port=" << sPort
                    << " caps=\"application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96\" ! rtph264depay ! decodebin ! videoconvert ! autovideosink sync=false" << endl
                    << "Dragonfly CAM_SOURCE: " << endl
                    << "-> gstreamer:udpsrc port=" << sPort
                    << " caps=\"application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96\" ! rtph264depay ! decodebin ! videoconvert ! appsink sync=false" << endl;
            }
            else
            {
                cout << "gstreamer command-line: " << endl
                    << "-> gst-launch-1.0 tcpclientsrc host=" << sIP << " port=" << sPort
                    << " ! gdpdepay ! rtph264depay ! avdec_h264 ! videoconvert ! autovideosink sync=false" << endl
                    << "Dragonfly CAM_SOURCE: " << endl
                    << "-> gstreamer:tcpclientsrc host=" << sIP << " port=" << sPort
                    << " ! gdpdepay ! rtph264depay ! avdec_h264 ! videoconvert ! appsink sync=false" << endl;
            }
        }
        else
        {
            if(sProtocol == std::string("udp"))
            {
                cout << "gstreamer command-line: " << endl
                    << "-> gst-launch-1.0 udpsrc port=" << sPort
                    << " ! application/x-rtp, encoding-name=JPEG,payload=26 ! rtpjpegdepay"
                    << " ! jpegparse ! jpegdec ! videoconvert ! autovideosink sync=false" << endl
                    << "Dragonfly CAM_SOURCE: " << endl
                    << "-> gstreamer:udpsrc port=" << sPort
                    << " ! application/x-rtp, encoding-name=JPEG,payload=26 ! rtpjpegdepay"
                    << " ! jpegparse ! jpegdec ! videoconvert ! appsink sync=false" << endl;
            }
            else
            {
                cout << "gstreamer command-line: " << endl
                        << "-> gst-launch-1.0 tcpclientsrc host=" << sIP << " port=" << sPort
                        << " ! multipartdemux ! image/jpeg, framerate=30/1 !"
                        << " jpegparse ! jpegdec ! videoconvert ! autovideosink sync=false" << endl
                        << "Dragonfly CAM_SOURCE: " << endl
                        << "-> gstreamer:tcpclientsrc host=" << sIP << " port=" << sPort
                        << " ! multipartdemux ! image/jpeg, framerate=30/1 !"
                        << " jpegparse ! jpegdec ! videoconvert ! appsink sync=false" << endl;
            }
        }
        
        cout << endl << "Quit with Ctrl+C" << endl;
    }

    cv::Mat mFrameLeft, mFrameRight, mFrameConcact;

    while (true)
    {
        vcl.grab();

        if (nRightIdx >= 0)
            vcr.grab();

        vcl.retrieve(mFrameLeft);

        if (nRightIdx >= 0)
            vcr.retrieve(mFrameRight);

        if (mFrameLeft.empty())
            break;

        if (nRightIdx >= 0 && mFrameRight.empty())
            break;

        if (nRightIdx < 0)
            streamer.write(mFrameLeft);
        else
        {
            cv::Mat mFrameConcact;
            cv::hconcat(mFrameLeft, mFrameRight, mFrameConcact);
            streamer.write(mFrameConcact);
        }
    }

    return 1;
}